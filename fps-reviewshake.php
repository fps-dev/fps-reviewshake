<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.frontporchsolutions.com/
 * @since             1.1
 * @package           Fps_Reviewshake
 *
 * @wordpress-plugin
 * Plugin Name:       fps-reviewshake
 * Plugin URI:        https://www.frontporchsolutions.com/
 * Description:       Plugin WordPress for reviewshake: The new standard in review management - A complete toolkit for smart companies that want to make online reviews work for them.
 * Version:           3.7.2
 * Author:            frontporchsolutions
 * Author URI:        https://www.frontporchsolutions.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       fps-reviewshake
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('FPS_REVIEWSHAKE_VERSION', '3.7.2');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-fps-reviewshake-activator.php
 */
function activate_fps_reviewshake()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-fps-reviewshake-activator.php';
	Fps_Reviewshake_Activator::activate();
	// Fps_Reviewshake_Activator::createPage();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-fps-reviewshake-deactivator.php
 */
function deactivate_fps_reviewshake()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-fps-reviewshake-deactivator.php';
	Fps_Reviewshake_Deactivator::deactivate();

	// Delete Cronjobs when deactivate plugin fps-reviewshake
	$getClientsApiCronValue = wp_next_scheduled('getClientsApiCron');
	$getReviewShakeApiCronValue = wp_next_scheduled('getReviewShakeApiCron');
	wp_unschedule_event($getClientsApiCronValue, 'getClientsApiCron');
	wp_unschedule_event($getReviewShakeApiCronValue, 'getReviewShakeApiCron');
}

register_activation_hook(__FILE__, 'activate_fps_reviewshake');
register_deactivation_hook(__FILE__, 'deactivate_fps_reviewshake');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-fps-reviewshake.php';

// Include Advanced custom fields in this plugin
require_once plugin_dir_path(__FILE__) . 'includes/getacf.php';

// Register Custom Post type reviewShake
require_once plugin_dir_path(__FILE__) . 'includes/cptfpsallreviewshake.php';

// Get information Clients get reviewShake
require_once plugin_dir_path(__FILE__) . 'includes/fpsclientsreviewshake.php';

// Create Shortcodes
require_once plugin_dir_path(__FILE__) . 'includes/shortcodes.php';

// Add link setting in list plugin view
require_once plugin_dir_path(__FILE__) . 'includes/settinglink.php';

// add plugin update checker
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/fps-dev/fps-reviewshake/',
	__FILE__, //Full path to the main plugin file or functions.php.
	'fps-reviewshake'
);


/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_fps_reviewshake()
{

	$plugin = new Fps_Reviewshake();
	$plugin->run();
}
run_fps_reviewshake();
