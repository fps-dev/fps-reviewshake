<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.frontporchsolutions.com/
 * @since      1.0.0
 *
 * @package    Fps_Reviewshake
 * @subpackage Fps_Reviewshake/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Fps_Reviewshake
 * @subpackage Fps_Reviewshake/includes
 * @author     @yavallejo <yan@frontporchsolutions.com>
 */
class Fps_Reviewshake_Activator
{

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate()
	{
	}

	public static function createPage()
	{
		// Insert page Reviews when enabled plugin
		$post = array(
			'post_content' => 'content', //content of page
			'post_title' => 'Reviews', //title of page
			'post_status' => 'publish', //status of page - publish or draft
			'post_type' => 'page' // type of post
		);
		if ($page_id = wp_insert_post($post))
		{
			// Only update this option if `wp_insert_post()` was successful
			update_option('reviews_id', $page_id);
		}
	}
}
