<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.frontporchsolutions.com/
 * @since      1.0.0
 *
 * @package    Fps_Reviewshake
 * @subpackage Fps_Reviewshake/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Fps_Reviewshake
 * @subpackage Fps_Reviewshake/includes
 * @author     @yavallejo <yan@frontporchsolutions.com>
 */
class Fps_Reviewshake_Deactivator
{

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate()
	{
	}

	public static function deletePage()
	{
		// Delete page reviews when disabled plugin
		$page_id = intval(get_option('reviews_id'));
		// Force delete this so the Title/slug "Menu" can be used again.
		wp_delete_post($page_id, true);
	}
}
