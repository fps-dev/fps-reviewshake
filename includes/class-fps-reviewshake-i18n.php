<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.frontporchsolutions.com/
 * @since      1.0.0
 *
 * @package    Fps_Reviewshake
 * @subpackage Fps_Reviewshake/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Fps_Reviewshake
 * @subpackage Fps_Reviewshake/includes
 * @author     @yavallejo <yan@frontporchsolutions.com>
 */
class Fps_Reviewshake_i18n
{


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain()
	{

		load_plugin_textdomain(
			'fps-reviewshake',
			false,
			dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
		);
	}
}
