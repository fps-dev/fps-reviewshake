<?php
function registerCpt_fps_reviewshake()
{
    register_post_type('cptfpsreviewshake', array(
        'labels' => array(
            'name'          => __('CPT All Reviews Shake', 'fps-reviewshake'),
            'singular_name' => __('CPT All Review Shake', 'fps-reviewshake')
        ),
        'public'            => true,
        'capability_type'   => 'post',
        'menu_icon'         => 'dashicons-superhero-alt',
        'supports'          => array('title'),
        'menu_position'     => 160
    ));
}
add_action('init', 'registerCpt_fps_reviewshake');

// Enable Column information dashboard
function addNewColumns_fps_reviewshake($column)
{
    $column['fps_source_name'] = 'Source Name';
    //Name of field .   //Show on the column
    unset($column['date']); //remove the date col
    return $column;
}
add_filter('manage_posts_columns', 'addNewColumns_fps_reviewshake');

function addNewColumnsValue_fps_reviewshake($column_name)
{
    if ($column_name == 'fps_source_name') {
        echo get_field('fps_source_name');
    }
}
add_action('manage_posts_custom_column', 'addNewColumnsValue_fps_reviewshake', 10, 1);

function flushCPT_fps_reviewshake()
{
    registerCpt_fps_reviewshake();
    flushCPT_fps_reviewshake();
}

register_activation_hook(__FILE__, 'flushCPT_fps_reviewshake');
register_uninstall_hook(__FILE__, 'cptUnistall_fps_reviewshake');

function cptUnistall_fps_reviewshake()
{
    // UnInstallation stuff here
    unregister_post_type('cptfpsreviewshake');
}

// Add fields to CPT
if (function_exists('acf_add_local_field_group')) :

    acf_add_local_field_group(array(
        'key' => 'group_6179b493b83b5',
        'title' => 'CPT Reviews Shake',
        'fields' => array(
            array(
                'key' => 'field_6179b52050cc7',
                'label' => 'Reviewer name',
                'name' => 'fps_reviewer_name',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '50',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_6179b53750cc8',
                'label' => 'Reviewer Profile Picture',
                'name' => 'fps_reviewer_profile_picture',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '50',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_6179b4d750cc6',
                'label' => 'Rating',
                'name' => 'fps_rating',
                'type' => 'number',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '10',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'min' => '',
                'max' => '',
                'step' => '',
            ),
            array(
                'key' => 'field_6179b4d150cc5',
                'label' => 'Url',
                'name' => 'fps_url',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '60',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_6179b5528d35b',
                'label' => 'Source Name',
                'name' => 'fps_source_name',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '20',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_6179b4b950cc2',
                'label' => 'Review Date',
                'name' => 'fps_review_date',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '10',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_6179b4c250cc3',
                'label' => 'Text',
                'name' => 'fps_text',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'cptfpsreviewshake',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));

endif;

// Settings Hooks
add_action('wp_ajax_nopriv_getReviewShakeApi', 'getReviewShakeApi');
add_action('wp_ajax_getReviewShakeApi', 'getReviewShakeApi');
add_action('getReviewShakeApiCron', 'getReviewShakeApi');

// The Event Function
function getReviewShakeApi()
{

    // First Delete all post from CPT
    $allReviewsCPT = get_posts(array(
        'post_type'      => 'cptfpsreviewshake',
        'posts_per_page' => -1
    ));

    foreach ($allReviewsCPT as $itemReviewCPT) {
        wp_delete_post($itemReviewCPT->ID, true); // Set to False if you want to send them to Trash.
    }

    // Function API
    $APIKEY     = get_field('api_key', 'option');
    $clientSlug = get_field('fps_client_slug', 'option');
    $limitReviews = 100;

    // Should return an array of objects
    $SUBDOMAIN   = get_field('subdomain', 'option') . "/api/v1/reviews?&client_slug={$clientSlug}&perPage={$limitReviews}";

    $args = array(
        'method' => 'GET',
        'timeout' => 20,
        'headers' => array(
            "Content-type" => "application/json",
            "X-Spree-Token" => $APIKEY
        ),
    );

    $response = wp_remote_get($SUBDOMAIN, $args);
    if (is_wp_error($response)) {
        // Either the API is down or something else spooky happened. Just be done.
        $error_message = $response->get_error_message();
        error_log("Error Message: " . $error_message);
        return false;
    } else {
        $results = json_decode(wp_remote_retrieve_body($response), true);
        $countReviews = count($results['reviews']);
        if ($countReviews <= $limitReviews) {
            $limit = $countReviews;
        } else {
            $limit = $limitReviews;
        }

        for ($i = 0; $i < $limit; $i++) {
            $reviewData = $results['reviews'][$i];
            $reviewID = $reviewData['id'];
            $reviewName = $reviewData['reviewer']['name'];
            $reviewDatePost = $reviewData['review_date'];
            $review_slug = sanitize_title($reviewName) . '-' . $reviewID;
            $existing_review = get_page_by_path($review_slug, 'OBJECT', 'cptfpsreviewshake');
            if ($existing_review === null) {
                $inserted_review = wp_insert_post([
                    'post_name'   => $review_slug,
                    'post_title'  => $review_slug,
                    'post_type'   => 'cptfpsreviewshake',
                    'post_status' => 'publish',
                    'post_date'   => $reviewDatePost
                ]);

                if (is_wp_error($inserted_review) || $inserted_review === 0) {
                    error_log('Could not insert reviews: ' . $review_slug);
                    continue;
                }

                // add meta fields
                $fillable = [
                    'field_6179b52050cc7' => 'reviewer',
                    'field_6179b53750cc8' => 'reviewer',
                    'field_6179b4d750cc6' => 'rating',
                    'field_6179b4d150cc5' => 'url',
                    'field_6179b5528d35b' => 'source_name',
                    'field_6179b4b950cc2' => 'review_date',
                    'field_6179b4c250cc3' => 'text',
                ];

                foreach ($fillable as $key => $name) {
                    if ($key === 'field_6179b52050cc7') {
                        update_field($key, is_array($reviewData[$name]) ? $reviewData[$name]['name'] : null, $inserted_review);
                    } elseif ($key === 'field_6179b53750cc8') {
                        update_field($key, is_array($reviewData[$name]) ? $reviewData[$name]['profile_picture'] : null, $inserted_review);
                    } else {
                        update_field($key, $reviewData[$name], $inserted_review);
                    }
                }
            } else {
                $existing_review_id = $existing_review->ID;
                $exisiting_review_timestamp = get_field('fps_review_date', $existing_review_id);

                if ($reviewDatePost >= $exisiting_review_timestamp) {

                    $fillable = [
                        'field_6179b52050cc7' => 'reviewer',
                        'field_6179b53750cc8' => 'reviewer',
                        'field_6179b4d750cc6' => 'rating',
                        'field_6179b4d150cc5' => 'url',
                        'field_6179b5528d35b' => 'source_name',
                        'field_6179b4b950cc2' => 'review_date',
                        'field_6179b4c250cc3' => 'text',
                    ];

                    foreach ($fillable as $key => $name) {
                        if ($key === 'field_6179b52050cc7') {
                            update_field($key, is_array($reviewData[$name]) ? $reviewData[$name]['name'] : null, $existing_review_id);
                        } elseif ($key === 'field_6179b53750cc8') {
                            update_field($key, is_array($reviewData[$name]) ? $reviewData[$name]['profile_picture'] : null, $existing_review_id);
                        } else {
                            update_field($key, $reviewData[$name], $existing_review_id);
                        }
                    }
                }
            }
        }
    }
    wp_die();
}

// Scheduling recurring event
if (!wp_next_scheduled('getReviewShakeApiCron')) {
    wp_schedule_event(time(), 'daily', 'getReviewShakeApiCron');
}
