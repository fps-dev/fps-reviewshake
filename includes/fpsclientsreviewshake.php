<?php
// Settings Hooks
add_action('wp_ajax_nopriv_getClientsApi', 'getClientsApi');
add_action('wp_ajax_getClientsApi', 'getClientsApi');
add_action('getClientsApiCron', 'getClientsApi');

// The Event Function
function getClientsApi()
{
    $idClient = get_field('client_slug', 'option');
    $APIKEY = get_field('api_key', 'option');
    $url = get_field('subdomain', 'option') . '/api/v1/clients/' . $idClient;
    $args = array(
        'method' => 'GET',
        'headers' => array(
            "Content-type" => "application/json",
            "X-Spree-Token" => $APIKEY
        ),
    );

    $response = wp_remote_post($url, $args);
    $results = json_decode(wp_remote_retrieve_body($response), true);
    // Either the API is down or something else spooky happened. Just be done.
    if (!is_array($results) || empty($results))
    {
        return false;
    }

    // Count number reviews Five start depending on select review_source field
    $REVIEWSOURCE  = get_field('review_source', 'option');
    $reviewsSource = $results['review_sources'];
    $valueReviews  = [];

    // We filter the selected sources and then sum them up.
    $filterSourceName = array_filter($reviewsSource, function ($var) use ($REVIEWSOURCE)
    {
        return in_array($var['name'], $REVIEWSOURCE);
    });

    $sunFiveStart = 0;
    foreach ($filterSourceName as $value)
    {
        $resultFivestart = intval($value['review_ratings']['rating_5']);
        $sunFiveStart += $resultFivestart;
    }

    // add meta fields
    $fillable = [
        'field_618c381f892aa' => 'id',
        'field_618c3836892ab' => 'name',
        'field_618c384a892ac' => 'slug',
        'field_618c3869892ad' => 'total_review_sources',
        'field_618433b58650e' => 'total_reviews',
        'field_618433b588gcv' => 'review_ratings',
        'field_618433c78650f' => 'average_rating',
        'field_618c39d2d663c' => 'review_sources',
        'field_618c3a02d663d' => 'name',
        'field_618c3a13d663e' => 'logo',
        'field_618c3a22d663f' => 'total_reviews',
        'field_618c3a34d6640' => 'average_rating',
    ];

    foreach ($reviewsSource as $source)
    {
        array_push($valueReviews, array(
            'field_618c3a02d663d' => $source['name'],
            'field_618c3a13d663e' => $source['logo'],
            'field_618c3a22d663f' => $source['total_reviews'],
            'field_618c3a34d6640' => $source['average_rating'] ? $source['average_rating'] : '0',
        ));
    }
    foreach ($fillable as $key => $name)
    {
        if ($key === 'field_618c39d2d663c')
        {
            $field_key = "field_618c39d2d663c";
            update_field($field_key, $valueReviews, 'option');
        }
        else
        {
            if ($key === 'field_618433b588gcv')
            {
                update_field($key, $sunFiveStart, 'option');
            }
            else
            {
                update_field($key, $results[$name], 'option');
            }
        }
    }

    // wp_die();
}

// Scheduling recurring event
if (!wp_next_scheduled('getClientsApiCron'))
{
    wp_schedule_event(time(), 'daily', 'getClientsApiCron');
}
