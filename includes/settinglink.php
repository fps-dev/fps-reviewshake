<?php // do not include this opening php tag in your file
add_filter('plugin_action_links_fps-reviewshake/fps-reviewshake.php', 'nc_settings_link');
function nc_settings_link($links)
{
    // Build and escape the URL.
    $url = esc_url(add_query_arg(
        'page',
        'fpsreviewshake',
        get_admin_url() . 'admin.php'
    ));
    // Create the link.
    $settings_link = "<a href='$url'>" . __('Settings') . '</a>';
    // Adds the link to the end of the array.
    array_push(
        $links,
        $settings_link
    );
    return $links;
}//end nc_settings_link()