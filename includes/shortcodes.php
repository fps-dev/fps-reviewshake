<?php
//Driving Directions
function driving_directions()
{
    $map = get_field('fps_address', 'option');
    if (empty($map)) {
        return;
    }
    $dec = $map['lat'];
    $vars = explode(".", $dec);
    $deg = $vars[0];
    $tempma = "0." . $vars[1];
    $tempma = $tempma * 3600;
    $min = floor($tempma / 60);
    $sec = $tempma - ($min * 60);
    $latdeg = $deg . '&#176;' . $min . '&prime;' . $sec . '&Prime;';
    //Long
    $lng = $map['lng'];
    $vars = explode(".", $lng);
    $deg = $vars[0];
    $tempma = "0." . $vars[1];
    $tempma = $tempma * 3600;
    $min = floor($tempma / 60);
    $sec = $tempma - ($min * 60);
    $longdeg = $deg . '&#176;' . $min . '&prime;' . $sec . '&Prime;';
    return  $latdeg . '+' . $longdeg . '/@' . $deg . ',' . $lng . '/';
}

// Add short code appearance start

if (false === get_option('reviewsAggregate_schema')) {
    // Option does not exist, generates empty option
    update_option('reviewsAggregate_schema', '');
}
function reviewsAggregate_fps_reviewshake($atts = [], $content = null)
{
    // Add Attribute for title and style layout
    $reviewsAggregate_atts = shortcode_atts(
        array(
            'title' => 'Reviews',
            'style' => 'row',
            'five-stars' => '',
        ),
        $atts,
    );

    // Get Value fields
    $PAGEREVIEWS = get_field('select_page_reviews', 'option');
    // We add up the total reviews of the selected sources
    $REVIEWSOURCE = get_field('review_source', 'option');
    $reviewSource = get_field('fps_client_review_sources', 'option');
    $reviewscount = 0;
    $averageTotal = 0;
    // We filter the selected sources and then sum them up.
    if (empty($reviewSource)) {
        return;
    }
    $filterSourceName = array_filter($reviewSource, function ($var) use ($REVIEWSOURCE) {
        return in_array($var['fps_client_repetear_name'], $REVIEWSOURCE);
    });

    // We scroll through the result to add it up
    foreach ($filterSourceName as $key => $value) {
        $reviewscount += (intval($value['fps_client_repetear_total_review']) * floatval($value['fps_client_repetear_average_rating']));
    }

    // we add up the average_rating per selected source
    foreach ($filterSourceName as $key => $value) {
        $averageTotal += intval($value['fps_client_repetear_total_review']);
    }

    if ($reviewsAggregate_atts['five-stars'] === 'yes' && !empty(get_field('fps_client_total_reviews_five_stars', 'option'))) {
        $aggregate = 5;
        $averageTotal = get_field('fps_client_total_reviews_five_stars', 'option');
        $resultAverage   = $reviewscount  / $averageTotal;
    } else {
        $resultAverage   = $reviewscount  / $averageTotal;
        $aggregate = number_format($resultAverage, 2);
    }
    $aggregateRound = round($aggregate * 2) / 2;
    $drawn = 5;

    // Get Data for create schema
    $fpsLogo        = get_field('fps_logo', 'option');
    $fpsName        = get_field('fps_name', 'option');
    $fpsBrand       = get_field('fps_brand', 'option');
    $fpsPhone       = get_field('fps_phone', 'option');
    $fpsPayment     = get_field('fps_payment_accepted', 'option');
    $fpsPriceRange  = get_field('fps_price_range', 'option');
    $fpsDescription = get_field('fps_description', 'option');
    $fpsAddress     = get_field('fps_address', 'option');
    if ($fpsAddress) {
        $address     = array_key_exists('address', $fpsAddress) ? $fpsAddress['address'] : ' ';
        $city        = array_key_exists('city', $fpsAddress) ? $fpsAddress['city'] : ' ';
        $country     = array_key_exists('country', $fpsAddress) ? $fpsAddress['country'] : ' ';
        $state_short = array_key_exists('state_short', $fpsAddress) ? $fpsAddress['state_short'] : ' ';
        $post_code   = array_key_exists('post_code', $fpsAddress) ? $fpsAddress['post_code'] : ' ';
        $latitude    = array_key_exists('lat', $fpsAddress) ? $fpsAddress['lat'] : ' ';
        $longitude   = array_key_exists('lng', $fpsAddress) ? $fpsAddress['lng'] : ' ';
    }

    $value = round($aggregate, 1, PHP_ROUND_HALF_UP);

    $reviewsAggregate_schema_data = array(
        'logo'        => esc_url(wp_get_attachment_url($fpsLogo)),
        'name'        => $fpsName,
        'url'         => get_bloginfo('url'),
        'hasMap'      => 'https://www.google.com/maps/place/' . driving_directions(),
        'brand'       => $fpsBrand,
        'phone'       => $fpsPhone,
        'payment'     => $fpsPayment,
        'priceRange'  => $fpsPriceRange,
        'description' => $fpsDescription,
        'address'     => $fpsAddress ? array(
            'address'     => $address,
            'city'        => $city,
            'country'     => $country,
            'state_short' => $state_short,
            'post_code'   => $post_code,
            'latitude'    => $latitude,
            'longitude'   => $longitude,
        ) : null, // if no address, set to null
        'aggregateRating' => array(
            'ratingValue' => number_format((float)$value, 1, '.', ''),
            'reviewCount' => $averageTotal,
        ),
    );

    update_option('reviewsAggregate_schema', $reviewsAggregate_schema_data);
    // Validate style layout in frontend
    if ($reviewsAggregate_atts['style'] === 'row') {
        $content = '<div class="fps-reviewBox">';
        $value = round($aggregate, 1, PHP_ROUND_HALF_UP);
        $content .= '<a class="fps-reviews" href="' . get_permalink($PAGEREVIEWS->ID) . '">
                        <strong class="fps-reviews--aggregate">' . number_format((float)$value, 1, '.', '') . '</strong>
                        <div class="fps-reviews--start">';
    } else {
        $value = round($aggregate, 1, PHP_ROUND_HALF_UP);
        $content = '<div>';
        $content .= '<div class="fps-reviews fps-reviews--column">
                        <strong class="fps-reviews--aggregate">' . number_format((float)$value, 1, '.', '') . '</strong>
                        <div class="fps-reviews--start">';
    }

    // full stars.
    for ($i = 0; $i < floor($aggregateRound); $i++) {
        $drawn--;
        $content .= '<div class="fps-star"><span class="fpsreview-star-full"></span></div>';
    }
    // half stars.
    if ($aggregate - floor($aggregateRound) === 0.5) {
        $drawn--;
        $content .= '<div class="fps-half-star"><span class="fpsreview-star-half"></span></div>';
    }
    // empty stars.
    for ($i = 0; $i < $drawn; $i++) {
        $content .= '<div class="fps-empty-star"><span class="fpsreview-star-empty"></span></div>';
    }

    if ($reviewsAggregate_atts['style'] === 'row') {
        $content .= '</div>
                        <div class="fps-total-reviews">
                            <span>' . number_format($averageTotal, 0, ',', ',') . '</span>
                            <span>' . $reviewsAggregate_atts['title'] . '</span>
                        </div>';
        $content .= '</a></div>';
    } else {
        $content .= '</div>
                        <div class="fps-total-reviews">
                            <span>' . number_format($averageTotal, 0, ',', ',') . '</span>
                            <span>' . $reviewsAggregate_atts['title'] . '</span>
                        </div>';
        $content .= '</div></div>';
    }

    // always return
    return $content;
}

// Function to print the schema data in the wp_head
function print_reviewsAggregate_schema()
{
    $reviewsAggregate_schema = get_option('reviewsAggregate_schema');
    if (!empty($reviewsAggregate_schema)) {
        $schema = array(
            '@context'        => 'https://schema.org',
            '@type'           => 'LocalBusiness',
            'name'            => $reviewsAggregate_schema['name'],
            'url'             => $reviewsAggregate_schema['url'],
            'telephone'       => $reviewsAggregate_schema['phone'],
            'image'           => $reviewsAggregate_schema['logo'],
            'logo'            => $reviewsAggregate_schema['logo'],
            'description'     => $reviewsAggregate_schema['description'],
            'priceRange'      => $reviewsAggregate_schema['priceRange'],
            'paymentAccepted' => $reviewsAggregate_schema['payment'],
            'aggregateRating' => array(
                '@type' => 'AggregateRating',
                'ratingValue' => $reviewsAggregate_schema['aggregateRating']['ratingValue'],
                'reviewCount' => $reviewsAggregate_schema['aggregateRating']['reviewCount'],
            ),
        );

        if (!empty($reviewsAggregate_schema['address'])) {
            $schema['address'] = array(
                '@type'           => 'PostalAddress',
                'streetAddress'   => $reviewsAggregate_schema['address']['address'],
                'addressLocality' => $reviewsAggregate_schema['address']['city'],
                'addressRegion'   => $reviewsAggregate_schema['address']['state_short'],
                'postalCode'      => $reviewsAggregate_schema['address']['post_code'],
                'addressCountry'  => $reviewsAggregate_schema['address']['country'],
            );
            $schema['geo'] = array(
                '@type'     => 'GeoCoordinates',
                'latitude'  => $reviewsAggregate_schema['address']['latitude'],
                'longitude' => $reviewsAggregate_schema['address']['longitude'],
            );
            $schema['hasMap'] = $reviewsAggregate_schema['hasMap'];
        }

        if (have_rows('fps_opening_hours', 'option')) {
            $schema['openingHours'] = array();
            while (have_rows('fps_opening_hours', 'option')) : the_row();
                $fps_hours = get_sub_field('fps_hours', 'option');
                $schema['openingHours'][] = $fps_hours;
            endwhile;
        }

        echo '<script type="application/ld+json">';
        echo json_encode($schema);
        echo '</script>';
    }
}

// Add Shortcode button write a review
function writeReview_fps_reviewshake($atts = [], $content = null)
{
    // Add Attribute for title, target and url
    $URLSITE = get_home_url('/');
    $writeReview_atts = shortcode_atts(
        array(
            'title' => 'Reviews',
            'target' => '_blank',
            'url' => $URLSITE
        ),
        $atts,
    );
    $content = "<a href='{$writeReview_atts['url']}' target='{$writeReview_atts['target']}' class='fps-btn' data-linktype='review'>
                    <span class='fpsreview-star-full'></span> {$writeReview_atts['title']}
                </a>";
    return $content;
}

// Shortcode view all our reviews
function allReview_fps_reviewshake($atts = [], $content = null)
{
    $allReview_atts = shortcode_atts(
        array(
            'title' => 'View All Our Reviews'
        ),
        $atts,
    );
    if (have_rows('company', 'option')) :
        $content = "<div class='fps-allReviews'><h2 class='fps-allReviews--title'>{$allReview_atts['title']}</h2>";
        $content .= "<div class='fps-allReviews--items'>";
        while (have_rows('company', 'option')) : the_row();
            $slugCompany = get_sub_field('company', 'option');
            $imageUrl = plugin_dir_url(__FILE__) . 'images/';

            $link = get_sub_field('link_review', 'option');
            $content .= "<a class='item' target='_blank' href='{$link}'>
                            <figure><img src='{$imageUrl}img-{$slugCompany}.png' alt='Logo {$slugCompany}'/></figure>
                         </a>";
        endwhile;
        $content .= "</div>";
        $content .= '</div>';
    endif;
    return $content;
}

// Shortcode reviews page and only configuration shortcode
function reviewsPage_fps_reviewshake($atts = [], $content = null)
{
    ob_start();
    // Generate Atts
    $reviewsPage_atts = shortcode_atts(
        array(
            'rating'         => 5,
            'initial_posts'  => 10,
            'loadmore_posts' => 10
        ),
        $atts
    );

    $additonalArr = array();
    $additonalArr['appendBtn'] = true;
    $additonalArr["offset"] = 0;

?>
    <div class="fpsAllPostsWrapper">
        <input type="hidden" name="rating" value="<?php echo $reviewsPage_atts['rating']; ?>">
        <input type="hidden" name="offset" value="0">
        <input type="hidden" name="fpsloadMorePosts" value="<?php echo $reviewsPage_atts['loadmore_posts']; ?>">
        <div class="fps-reviewsPage">
            <?php fpsGetPostsFtn($reviewsPage_atts, $additonalArr); ?>
        </div>
    </div>
    <?php
    return ob_get_clean();
}
// Template for item reviews - this function is used in reviewsPage_fps_reviewshake
function fpsGetPostsFtn($reviewsPage_atts, $additonalArr = array())
{

    // Get atts value
    $initialPost = $reviewsPage_atts['initial_posts'];
    $RATING      = $reviewsPage_atts['rating'];
    $offset      = $additonalArr["offset"];

    // Get value ACF FPS Review Platform Setup
    $sourceName = get_field('review_source', 'options');
    // Generate query custom to CPT
    $args = array(
        'post_type'      => 'cptfpsreviewshake',
        'posts_per_page' => $initialPost,
        'offset'         => $offset,
        'meta_query'     => array(
            'relation'   => 'AND',
            'rating'     => array(
                'key'    => 'fps_rating',
                'value'  => $RATING,
                'compare' => '>=',
            ),
            'sourcename' => array(
                'key'    => 'fps_source_name',
                'value'  => $sourceName,
                'compare' => 'IN',
            ),
            'textreview' => array(
                'key'    => 'fps_text',
                'value'  => '',
                'compare' => '!='
            ),
            'textreviewgoogledefault' => array(
                'key'    => 'fps_text',
                'value'  => 'A new Google review was added.',
                'compare' => '!='
            ),
        ),
    );

    $query = new WP_Query($args);
    $havePosts = true;
    if ($query->have_posts()) : ?>
        <?php $i = 0; ?>
        <?php while ($query->have_posts()) : $query->the_post(); ?>
            <?php
            $date = DateTime::createFromFormat('Y-m-d', get_field('fps_review_date'));
            $profilePicture = !is_null(get_field('fps_reviewer_profile_picture')) ? get_field('fps_reviewer_profile_picture') : plugins_url('../public/images/user-placeholder.png', __FILE__);
            $profileName = get_field('fps_reviewer_name');
            $profileUrl = get_field('fps_url');
            $profileDescription = get_field('fps_text');
            $contentReview = strlen($profileDescription);
            $reviewRating = get_field('fps_rating');
            ?>
            <div class="fps-item-review">
                <div class="fps-reviewer">
                    <?php
                    $isFacebook = get_field('fps_source_name');
                    if ($isFacebook === 'Facebook') { ?>
                        <img class="fps-imageProfile" alt="Image of <?php echo $profileName; ?>" src="<?php echo plugins_url('../public/images/user-placeholder.png', __FILE__); ?>" />
                    <?php } else { ?>
                        <img class="fps-imageProfile" alt="Image of <?php echo $profileName; ?>" src="<?php echo $profilePicture; ?>" />
                    <?php
                    }
                    ?>
                    <?php
                    if (!is_null($profileUrl)) { ?>
                        <a class="fps-nameProfile fps-nameProfile--link" href="<?php echo $profileUrl; ?>" target="_blank">
                            <?php echo strtolower($profileName); ?>
                        </a>
                        <a href="<?php echo $profileUrl ?>" target="_blank" class="fps-reviewer--verified">
                            <img src="<?php echo plugins_url('../public/images/verified.png', __FILE__); ?>" width="24" height="24" alt="icon verified" />
                        </a>
                    <?php
                    } else { ?>
                        <span class="fps-nameProfile"><?php echo strtolower($profileName) ?></span>
                        <img src="<?php echo plugins_url('../public/images/verified.png', __FILE__); ?>" width="20" height="20" alt="icon verified" />
                    <?php
                    }
                    ?>
                </div>
                <div class="fps-rating">
                    <div class="fps-reviews--start">
                        <?php
                        for ($x = 1; $x <= $reviewRating; $x++) { ?>
                            <div class="fps-star">
                                <span class="fpsreview-star-full"></span>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="fps-date"><?php echo $date->format('F d, Y'); ?></div>
                </div>
                <div class="fps-reviewer--content">
                    <p><?php echo $profileDescription; ?></p>
                    <?php
                    if ($contentReview >= 165 && wp_is_mobile()) { ?>
                        <a href="javascript:void(0)" class="fps-readMore">Read more</a>
                    <?php
                    } elseif ($contentReview >= 260) { ?>
                        <a href="javascript:void(0)" class="fps-readMore">Read more</a>
                    <?php
                    } ?>
                </div>
            </div><!-- Item Review-->
            <?php
            $i++;
            if ($i == $initialPost) {
                break;
            }
            ?>
        <?php endwhile; ?>
    <?php else :
        $havePosts = false; ?>
    <?php
    endif;
    wp_reset_query();
    // Validate for more information
    if ($havePosts && $additonalArr['appendBtn']) {
    ?>
        <div class="btnLoadmoreWrapper button-load">
            <button type="button" class="btn btn-primary fpsLoadMorePostsbtn">Load more reviews</button>
        </div>
        <div class="fpsLoaderImg" style="display: none;">
            <svg version="1.1" id="L9" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve" style="color: #ff7361;">
                <path fill="#ff7361" d="M73,50c0-12.7-10.3-23-23-23S27,37.3,27,50 M30.9,50c0-10.5,8.5-19.1,19.1-19.1S69.1,39.5,69.1,50">
                    <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="1s" from="0 50 50" to="360 50 50" repeatCount="indefinite"></animateTransform>
                </path>
            </svg>
        </div>
        <p class="fps-noMorePostsFound" style="display: none;">No More Posts Found</p>
    <?php
    }
}

// Ajax for item review button load more
function fpsAjaxLoadMorePostsAjaxReq()
{
    extract($_POST);
    $additonalArr = array();
    $additonalArr['appendBtn'] = false;
    $additonalArr["offset"] = $offset;
    $reviewsPage_atts["initial_posts"] = $fpsloadMorePosts;
    $reviewsPage_atts["rating"] = $fpsrating;
    fpsGetPostsFtn($reviewsPage_atts, $additonalArr);
    die();
}

add_action("wp_ajax_fpsAjaxLoadMorePostsAjaxReq", "fpsAjaxLoadMorePostsAjaxReq");
add_action("wp_ajax_nopriv_fpsAjaxLoadMorePostsAjaxReq", "fpsAjaxLoadMorePostsAjaxReq");


// Shortcode Home reviews rotator
function homeReviews_fps_reviewshake($atts = [], $content = null)
{
    ob_start();
    // Generate Atts
    $homeReviews_atts = shortcode_atts(
        array(
            'posts_per_page' => 6,
            'rating'         => 5,
            'style'          => 'one_column'
        ),
        $atts,
    );

    // Get Atts value
    $PAGINATION = $homeReviews_atts['posts_per_page'];
    $RATING = $homeReviews_atts['rating'];
    // Get value ACF FPS Review Platform Setup
    $sourceName = get_field('review_source', 'options');
    // Generate query custom to CPT
    $args = array(
        'posts_per_page' => $PAGINATION,
        'post_type'   => 'cptfpsreviewshake',
        'meta_query' => array(
            'relation' => 'AND',
            'rating' => array(
                'key'     => 'fps_rating',
                'value'   => $RATING,
                'compare' => '>=',
            ),
            'sourcename' => array(
                'key'     => 'fps_source_name',
                'value'   => $sourceName,
                'compare' => 'IN',
            ),
            'textreview' => array(
                'key' => 'fps_text',
                'value' => '',
                'compare' => '!='
            ),
            'textreviewgoogledefault' => array(
                'key' => 'fps_text',
                'value' => 'A new Google review was added.',
                'compare' => '!='
            ),
        ),
    );

    $query = new WP_Query($args);
    if ($query->have_posts()) : ?>
        <?php if ($homeReviews_atts['style'] === 'one_column') {
        ?>
            <div class='carouselReview carouselReview--one carousel-review'>
                <?php $i = 0; ?>
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <?php
                    $dateF = new DateTime(get_field('fps_review_date'));
                    $textReview = get_field('fps_text');
                    $ratingReview = get_field('fps_rating');
                    ?>
                    <div class="fps-item-review">
                        <div class="fps-content-review">
                            <h2 class="fps-content-review--description">
                                <?php echo substr($textReview, 0, 200) ?>
                            </h2>
                            <?php
                            $sourceName = get_field('fps_source_name');
                            $nameAuthor =  get_field('fps_reviewer_name');
                            $profileAuthor = get_field('fps_reviewer_profile_picture');
                            ?>
                            <a class="fps-item--link" href="<?php the_field('fps_url'); ?>" target="_blank">
                                Read Full Review on <?php echo $sourceName; ?>
                            </a>
                            <?php
                            if (($sourceName !== 'Facebook') && !is_null($profileAuthor)) {
                            ?>
                                <img class="fps-imageProfile fps-imageProfile--home" src="<?php echo $profileAuthor; ?>" width="60" height="24" alt="Image of <?php echo $nameAuthor; ?>" />
                            <?php
                            } else {
                            ?>
                                <img class="fps-imageProfile fps-imageProfile--home" width="60" height="24" src="<?php echo plugins_url('../public/images/user-placeholder.png', __FILE__); ?>" alt="Image of <?php echo $nameAuthor; ?>" />
                            <?php
                            }
                            ?>
                            <strong class="fps-nameProfile fps-d-block">
                                <?php echo strtolower($nameAuthor); ?>
                            </strong>
                            <span class="fps-date fps-ml-0">
                                <?php echo $dateF->format('m/d/Y'); ?>
                            </span>
                            <div class="fps-reviews--start fps-justify-content-center">
                                <?php
                                for ($x = 1; $x <= $ratingReview; $x++) {  ?>
                                    <div class="fps-star">
                                        <span class="fpsreview-star-full"></span>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    if ($i == $PAGINATION) {
                        break;
                    }
                    ?>
                    <?php $i++; ?>
                <?php endwhile; ?>
            </div>
        <?php
        } else { ?>
            <div class='carouselReview carouselReview--three'>
                <?php $i = 0; ?>
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                    <?php
                    $dateF = new DateTime(get_field('fps_review_date'));
                    $textReview = get_field('fps_text');
                    $ratingReview = get_field('fps_rating');
                    $nameAuthor =  get_field('fps_reviewer_name');
                    ?>
                    <div class="fps-item-review">
                        <div class="fps-content-review">
                            <h2 class="fps-content-review--description">
                                <?php echo substr($textReview, 0, 200) ?>
                            </h2>
                            <strong class="fps-nameProfile fps-d-block">
                                <?php echo strtolower($nameAuthor); ?>
                            </strong>
                            <div class="fps-reviews--start">
                                <?php
                                for ($x = 1; $x <= $ratingReview; $x++) {  ?>
                                    <div class="fps-star">
                                        <span class="fpsreview-star-full"></span>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                            <span class="fps-date">
                                <?php echo $dateF->format('m/d/Y'); ?>
                            </span>
                        </div>
                    </div>
                    <?php
                    if ($i == $PAGINATION) {
                        break;
                    }
                    ?>
                    <?php $i++; ?>
                <?php endwhile; ?>
            </div>
        <?php }
        ?>
<?php
    endif;
    wp_reset_query();
    return ob_get_clean();
}

// All shortcodes create
function wporg_shortcodes_init()
{
    $fpsSchemaAdd   = get_field('fps_schema_local', 'option');
    if ($fpsSchemaAdd) {
        add_action('wp_head', 'print_reviewsAggregate_schema');
    }
    add_shortcode('reviewsAggregate', 'reviewsAggregate_fps_reviewshake');
    add_shortcode('writeReview', 'writeReview_fps_reviewshake');
    add_shortcode('allReview', 'allReview_fps_reviewshake');
    add_shortcode('pageReview', 'reviewsPage_fps_reviewshake');
    add_shortcode('rotator', 'homeReviews_fps_reviewshake');
}

add_action('init', 'wporg_shortcodes_init');
