<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.frontporchsolutions.com/
 * @since      1.0.0
 *
 * @package    Fps_Reviewshake
 * @subpackage Fps_Reviewshake/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Fps_Reviewshake
 * @subpackage Fps_Reviewshake/public
 * @author     @yavallejo <yan@frontporchsolutions.com>
 */
class Fps_Reviewshake_Public
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Fps_Reviewshake_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Fps_Reviewshake_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_style('slickCSS', plugins_url('../public/css/vendor/slick.min.css', __FILE__), null, '1.8', 'all');
		wp_enqueue_style('slickThemeCSS', plugins_url('../public/css/vendor/slick-theme.min.css', __FILE__), null, '1.8', 'all');
		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/fps-reviewshake-public.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Fps_Reviewshake_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Fps_Reviewshake_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script('slickJS', plugins_url('../public/js/vendor/slick.min.js', __FILE__), array(), '1.8', true);
		wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/fps-reviewshake-public.js', array('jquery'), $this->version, true);
		wp_localize_script($this->plugin_name, 'fps_frontend_ajax_object', array('ajaxurl' => admin_url('admin-ajax.php')));
	}
}