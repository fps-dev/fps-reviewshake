(function ($) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(document).ready(function () {
		let ajaxurl = fps_frontend_ajax_object.ajaxurl;
		let fpsrating = parseInt($('input[name="rating"]').val());
		let currentPost = document.querySelectorAll('.fps-item-review').length; // initialPost
		$(document).on('click', '.fpsLoadMorePostsbtn', function () {
			currentPost = document.querySelectorAll('.fps-item-review').length;
			let fpsloadMorePosts = parseInt($('input[name="fpsloadMorePosts"]').val());
			$('.btnLoadmoreWrapper').hide();
			$.ajax({
				type: "POST",
				url: ajaxurl,
				data: ({
					action: "fpsAjaxLoadMorePostsAjaxReq",
					fpsrating: fpsrating,
					fpsloadMorePosts: fpsloadMorePosts,
					offset: currentPost
				}),
				success: function (response) {
					if (!$.trim(response)) {
						// blank output
						$('.fps-noMorePostsFound').show();
					} else {
						// append to last div
						$(response).insertAfter($('.fps-item-review').last());
						$('input[name="offset"]').val(currentPost);
						$('.btnLoadmoreWrapper').show();
					}
				},
				beforeSend: function () {
					$('.fpsLoaderImg').show();
				},
				complete: function () {
					$('.fpsLoaderImg').hide();
				},
				error: function (error) {
					console.log("Can't do because: " + error);
				}
			});
		});

		// Read More Shortcode [pageReview]
		if ($('.fps-readMore').length) {
			$(document).on("click", '.fps-readMore', function (event) {
				event.preventDefault();
				let descriptionReadMore = $(event.target.parentNode.firstChild.nextSibling);
				let targetanchor = $(event.target);
				descriptionReadMore.addClass('initial');
				targetanchor.hide();
			});
		}
	});

	// Carousel Home
	let $oneColumn = $(".carouselReview--one") || null;
	let $threeColumn = $(".carouselReview--three") || null;

	if ($oneColumn) {
		$($oneColumn).slick({
			lazyLoad: "progressive",
			arrows: true,
			infinite: true,
			slidesToShow: 1,
			dots: false,
			slidesToScroll: 1,
			mobileFirst: true,
			autoplaySpeed: 3000,
			autoplay: true,
			speed: 500,
		});
	}
	if ($threeColumn) {
		$($threeColumn).slick({
			lazyLoad: "progressive",
			arrows: true,
			infinite: true,
			slidesToShow: 1,
			dots: false,
			slidesToScroll: 1,
			mobileFirst: true,
			autoplay: true,
			autoplaySpeed: 5000,
			speed: 3000,
			responsive: [
				{
					breakpoint: 1100,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
	}
})(jQuery);
