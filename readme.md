# FPS ReviewShake

Plugin to connect to the reviewshake platform in a simple way.

## How to install

-   [Download plugin](https://bitbucket.org/fps-dev/fps-reviewshake/src/master/)
-   Extract to zip
-   Rename folder by: fps-reviewshake
-   Compress in zip
-   Upload plugin
-   Activate plugin

## Setup initial - FPS Review Platform

-   **Configure subdomain:** normally https://experience.frontporchsolutions.com
-   **Api key:** Request the api key of the project from the project manager
-   **Select page reviews:** Select the page where all reviews will be displayed
-   **Google maps Api key:** Request the api key of the project from the project manager

## Setup initial: FPS Review Platform Setup

-   **Review sources:** Select all the ones you need to display on the website
-   **ID Client:** Request the id from the project manager
-   **View all our reviews:** In this section we press the 'add company' button to select the company we need to display and we add the url
-   **Information Local Business:** Insert all the corresponding information, this information is used to create the structure requested by the localbusiness schemas.

## FPS Information Client **Important step**

Here we must obtain customer information which is **important** to continue with the use of reviews within the website.

to obtain the information we must do the following procedure

-   We must add this to the url of our site `admin-ajax.php?action=getClientsApi`
    Example: `http://example.com/wp-admin/admin-ajax.php?action=getClientsApi`

If the result is satisfactory it should automatically lead you to the following url `http://example.com/wp-admin/admin.php?page=fpsreviewshakeclient` and should display the following information:

-   ID
-   Name
-   Slug
-   Total Reviews Sources
-   Total Reviews
-   Average Rating
-   Review Sources
    -   Name
    -   Logo
    -   Total Review
    -   Average Rating

Note: for everything to work correctly you should have inserted the client id previously, if you have not inserted it and you make the request for the information with the `action=getClientsApi` you will get an error.

## CPT All Reviews Shake **Important step**

**THIS STEP SHOULD BE PERFORMED AFTER YOU FINISH THE STEP-> FPS Information Client**

In this step what we will do is to get all the information from the reviewShake api and save it in the WordPress database in the custom post type called **CPT All Reviews Shake**.

We have to do the following.

-   We must add this to the url of our site `admin-ajax.php?action=getReviewShakeApi`
    Example: `http://example.com/wp-admin/admin-ajax.php?action=getReviewShakeApi`

If the result is satisfactory it should automatically lead you to the following url `http://example.com/wp-admin/admin.php?post_type=cptfpsreviewshake` and should all reviews.

### Yoast SEO for CPT All Reviews Shake

When we finish the insertion of our reviews in the **CPT All Reviews Shake** we must verify if the Yoast SEO plugin exists in the project. If it exists we must deactivate it from appearing in the google search results.

![Yoast SEO](https://i.ibb.co/3s3bLfP/yoast-seo.png)

## Shortcode Available

-   Home Rotator: `[rotator]`
    -   Defaults Values:
        -   **posts_per_page=** 6
        -   **rating=** 5
        -   **style=** "one_column"
    -   Options :
        -   **Setup number post per page:** `[rotator posts_per_page=6]`
        -   **Setup rating:** `[rotator rating=1]` **Minimum:** 1 **Maximum:** 5
        -   **Setup Style:** `[rotator style="box"]`
    -   Example
        -   `[rotator posts_per_page=50 rating=4 style="box"]`

#### Style one_column

![Home Rotator](https://i.ibb.co/jHf81Xd/rotator-review.png)

#### Style Box

![Home Rotator style box](https://i.ibb.co/HxcX8y8/Screen-Shot-2021-09-27-at-4-01-15-PM.png)

-   Page Review: `[pageReview]`
    -   Defaults Values:
        -   **rating=** 5
        -   **initial_posts=** 10
        -   **loadmore_posts=** 10
    -   Options :
        -   **Setup rating:** `[pageReview rating=1]` **Minimum:** 1 **Maximum:** 5
        -   **initial_posts: set the number of reviews to display:** `[pageReview initial_posts=6]`
        -   **loadmore_posts: set the number of reviews to be displayed each time you click on the 'load more reviews' button:** `[pageReview loadmore_posts=1]` **Minimum:** 1 **Maximum:** 5
    -   Example
        -   `[pageReview rating=4 initial_posts=1 loadmore_posts=1]`

![Page review](https://i.ibb.co/W64S6Zq/page-reviews.png)

-   Reviews Aggregate Layout `[reviewsAggregate]`
    -   Defaults Values:
        -   **style=** Row
    -   Options :
        -   **Show Only Five Stars Reviews:** `[reviewsAggregate five-stars="yes"]`
        -   **Style Column:** `[reviewsAggregate style="column"]` ![Style column](https://i.ibb.co/kcPC4qq/rating-columns.png)
        -   **Style Row:** `[reviewsAggregate style="row"]` ![Style column](https://i.ibb.co/zbxXzy7/rating-row.png)
-   Button Write a review `[writeReview]`
    -   Defaults Values:
        -   **title=** Write a review
        -   **target=** \_blank
        -   **url=** https://www.google.com
    -   Options:
        -   **title:** `[writeReview title="FPS Plugin"]`
        -   **target:** `[writeReview target="_blank|_self|_parent|_top|framename"]`
    -   Example:
        -   `[writeReview title="FPS Plugin button" target="_self" url="https://www.frontporchsolutions.com/"]`
        -   ![Btn Rewrite](https://i.ibb.co/fHNxxQ7/btn-review.png)
-   View All Our Reviews `[allReview]`
    -   ![View All our reviews](https://i.ibb.co/rFgyNbr/Screen-Shot-2021-10-05-at-12-35-06-PM.png)

## Add sticky to Box reviews

    - There is a class called `.fps-sticky` which allows us to make any element sticky.
